const express = require('express');
const openSigningCeremony = require('./startSigningCeremony');

const app = express();

// App routes
app.get('/', (req, res) => {
  openSigningCeremony();
  res.send('<h1>Docusign experiment app</h1>')
});

// Listen to requests
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`)
});