const docusign = require('docusign-esign');
const getDocument = require('./signingCeremonyUtils').getDocument;
const getApiClient = require('./signingCeremonyUtils').getApiClient;
const { promisify } = require('util');

// Prepare constants
// @TODO to make this demo work
// 1) Get temporary access token from docusign // https://developers.docusign.com/oauth-token-generator
// 2) Put in your accountId // https://admindemo.docusign.com/admin-dashboard (click your initials/photo in the top right corner and copy the 7 digit number without the hashtag)
const accessToken = "";
const accountId = 'XXXXXXX';
const basePath = 'https://demo.docusign.net/restapi';
const authenticationMethod = 'None';
const baseUrl = 'http://localhost:5000';

const openSigningCeremony = async () => {
  // Create client (Patient in our case)
  const clientUserId = '123';
  const clientUserName = 'Sign Er';
  const clientUserEmail = 'sign.er@example.com';

  // Prepare definitions
  const signHere = docusign.SignHere.constructFromObject({
    documentId: '1',
    pageNumber: '1',
    recipientId: '1',
    tabLabel: 'SignHereTab',
    xPosition: '195',
    yPosition: '147',
  });

  const signer = docusign.Signer.constructFromObject({
    name: clientUserName,
    email: clientUserEmail,
    routingOrder: '1',
    recipientId: '1',
    clientUserId,
    tabs: docusign.Tabs.constructFromObject({ signHereTabs: [signHere] })
  });

  const document = getDocument('./mockContract.pdf');
  const envelopeDefinition = new docusign.EnvelopeDefinition.constructFromObject({
    emailSubject: 'Please sign this document sent from the Node example',
    emailBlurb: 'Please sign this document sent from the Node example.',
    documents: [document],
    status: 'sent',
    recipients: docusign.Recipients.constructFromObject({
      signers: [signer],
    })
  });

  const recipientViewRequest = docusign.RecipientViewRequest.constructFromObject(
    {
      authenticationMethod,
      clientUserId,
      recipientId: '1',
      returnUrl: baseUrl + '/dsreturn',
      userName: clientUserName,
      email: clientUserEmail,
    }
  );

  // Prepare ApiClients
  const apiClient = getApiClient(basePath, accessToken);
  docusign.Configuration.default.setDefaultApiClient(apiClient);
  const envelopesApi = new docusign.EnvelopesApi();

  // Prepare promises
  const createEnvelopePromise = promisify(
    envelopesApi.createEnvelope
  ).bind(envelopesApi);

  const createRecipientViewPromise = promisify(
    envelopesApi.createRecipientView
  ).bind(envelopesApi);

  // Execute requests
  try {
    const envelope = await createEnvelopePromise(accountId, {
      envelopeDefinition,
    });

    const envelopeId = envelope.envelopeId;
    const recipientView = await createRecipientViewPromise(accountId, envelopeId, {
      recipientViewRequest,
    });

    // This is what we send back to FE
    console.log('results.url', recipientView.url);
    return recipientView.url;
  } catch (error) {
    console.log(error)
  }
};

module.exports = openSigningCeremony;
