# Docusign experiment for CA

## How to operate
1) `npm install`
2) Set up constants in `./src/startSigningCeremony.js`
2) `npm run dev`
3) open `http://localhost:5000/` with your browser

## Documentation for docusign
https://developers.docusign.com/esign-rest-api/code-examples/quickstart-request-signature-embedded