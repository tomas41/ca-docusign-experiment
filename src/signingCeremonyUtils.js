const docusign = require('docusign-esign');
const fs = require('fs');
const path = require('path');

const getDocument = (filePath) => {
  const pdfBytes = fs.readFileSync(path.resolve(__dirname, filePath));
  const pdfBase64 = pdfBytes.toString('base64');

  return docusign.Document.constructFromObject({
    documentBase64: pdfBase64,
    fileExtension: 'pdf',
    name: 'A very important document',
    documentId: '1',
  });
};

const getApiClient = (basePath, accessToken) => {
  const apiClient = new docusign.ApiClient();
  apiClient.setBasePath(basePath);
  apiClient.addDefaultHeader('Authorization', 'Bearer ' + accessToken);

  return apiClient;
};

module.exports.getDocument = getDocument;
module.exports.getApiClient = getApiClient;